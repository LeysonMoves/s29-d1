// require express module and save it in a constant
const express = require("express");

// Create an application using express
const app = express();

// App server will listen to port 3000
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows the app to read json data
app.use(express.json());

// Allows the app t o read data from forms
app.use(express.urlencoded({extended:true}));

// GET route

app.get("/", (req, res) => {
	res.send("Hello World");
})

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
})


// POST route
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

// Mock Database
let users = [];

// Will create a signup post
app.post("/signup", (req, res) => {
	console.log(req.body);
	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);
	}
	else{
		res.send("Please input BOTH unsername and password");
	}
})

// PUT request for changing the password
app.put("/change-password", (req, res) =>{
	// Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop tha will loo through the elements of the "users" array
	for(let i=0; i <users.length; i++){

		if(req.body.username === users[i].username){

			users[i].password = req.body.password;
				
			message = `user ${req.body.username}'s password has been updated.`;
			break;	
		}
		else{
			message = "User does not exist"
		}
	}

	res.send(message);
})
// ---------------------------------
// Number 1 and 2- create GET route that will acccess the "/home" and process using postman
app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
})

// ---------------------------------------------
// NUMBER 3 AND 4 - create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// Processed using postman

app.get("/users", (req, res) => {
		res.send(users);
})
// ----------------------------------------------------------

// NUMBER 5 - Create DELETE request at the "/delete-user" route to remove user from the database
// NUMBER 6 - Process DELETE request at the "/delete-user" route using postman.
app.delete("/delete-user", (req, res) =>{
	let notice;
	for(let i=0; i <users.length; i++){
		if(req.body.username ==users[i].username){
			users.splice(i,1);
			notice = `User ${req.body.username} has been deleted.`;
			break;	
		}
		else{
			notice = "User does not exist, please try another username"
		}
	}
	res.send(notice);
})


// Tells the server to listen to the port
app.listen(port, () => console.log(`Server is running at port ${port}`));


















/*
app.get("/users", (req, res) => {
	let usersArray =[];
	for(let i = 0; <users.length; i++){
		usersArray.push(users[i]);
	}
	res.send(usersArray	);
})*/

